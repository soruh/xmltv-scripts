#!/bin/sh -e

temp="$(mktemp -d)"
prev="$PWD"
cd "$temp"
mkfifo video-MP2T-1
(cat video-MP2T-1 | dvbtee -Ee -j 2>&1 | grep --binary-files=text NET_SVC_ID | cut -d' ' -f2-)&
pid="$!"
openRTSP -V -d 20 "$1"
wait $pid
cd "$prev"
rm -r "$temp"
