const readline = require('readline');
const child_process = require('child_process');

const rl = readline.createInterface({
  input: process.stdin,
  terminal: false
});

let guide = {};


rl.on('line', (line) => {
    let tables = JSON.parse(line);

    let eits = tables.filter(tab => tab.tableName == 'EIT');
    let sdt = tables.find(tab => tab.tableName == 'SDT');

    for (let eit of eits) {
        let service = sdt?.services.find(service => service.serviceId == eit.serviceId);

        if (!service) {
            if (guide[eit.serviceId]) {
                service = guide[eit.serviceId].service;
            } else {
                process.stderr.write(`--- skipping EIT data for service ${eit.serviceId}; Missing SDT and unknown service\n`);
                continue;
            }
        }

        let descriptor = service.descriptors.find(desc => desc.descriptorTag = 72);

        let data = {
            name: descriptor.serviceName,
            serviceId: service.serviceId,
            eit,
            service,
        };
        
        if (!guide[data.serviceId]) guide[data.serviceId] = {
            name: data.name,
            service,
            serviceId: data.serviceId,
            events: {},
        };
        let channel = guide[data.serviceId].events;

        process.stderr.write(`--- parsing EIT data for ${data.name}\n`);
        
        for (let event of data.eit.events) {
            let { name, text, lang } = event.descriptors.find(desc => desc.descriptorTag == 77);
            
            let genre = text || null;
            let description = event.descriptors.filter(desc => desc.descriptorTag == 78).map(desc => desc.text).join("").trim() || null;
            let { unixTimeBegin, unixTimeEnd, eventId } = event;
            
            event = { name, genre, description, unixTimeBegin, unixTimeEnd, lang };

            if (channel[eventId]) {
                if (!channel[eventId].description || event.description) {
                    channel[eventId].description = event.description;
                } else if (!channel[eventId].genre || event.genre) {
                    channel[eventId].genre = event.genre;
                }
            } else {
                channel[eventId] = event;
            }
        }
    }
});

const escapeXml = unsafe => unsafe.replace(/[<>&'"]/g, c => {
    switch (c) {
        case '<': return '&lt;';
        case '>': return '&gt;';
        case '&': return '&amp;';
        case '\'': return '&apos;';
        case '"': return '&quot;';
    }
});


function format_time(unixTime) {
    let d = new Date(unixTime * 1000);
    let tzo = -d.getTimezoneOffset();
    let time_zone = (tzo >= 0 ? '+' : '') + Math.floor(tzo / 60).toString().padStart(2, 0) + (tzo % 60).toString().padStart(2, 0);

    let hour = d.getHours().toString().padStart(2, 0);
    let minute = d.getMinutes().toString().padStart(2, 0);
    let second = d.getSeconds().toString().padStart(2, 0);

    return `${format_date(unixTime)}${hour}${minute}${second} ${time_zone}`;
}


function format_date(unixTime) {
    let d = new Date(unixTime * 1000);
    let year = d.getFullYear().toString().padStart(4, 0);
    let month = (d.getMonth() + 1).toString().padStart(2, 0);
    let day = d.getDate().toString().padStart(2, 0);

    return `${year}${month}${day}`;
}

const icon_mapping = {
    '32': 'sonnenklar',
    '40': 'hse24',
    '48': 'n24',
    '60': 'sat1',
    '201': 'pro7maxx',
    '245': 'servus_tv',
    '764': 'anixe_sd',
    '776': 'disney_channel',
    '4900': 'anixe_hd',
    '10301': 'das_erste_hd',
    '10302': 'arte_hd',
    '10304': 'swr_hd',
    '10351': 'rbb',
    '10355': 'hr_hd',
    '10375': 'tagesschau24_hd',
    '10376': 'ard_one_hd',
    '11110': 'zdf_hd',
    '11130': 'zdf_neo_hd',
    '11150': '3sat_hd',
    '11160': 'kika_hd',
    '11170': 'zdf_info_hd',
    '12003': 'rtl',
    '12020': 'rtl2',
    '12030': 'rtl2',
    '12040': 'superrtl',
    '12060': 'vox',
    '12061': 'nitro',
    '12090': 'ntv',
    '12122': 'bibel_tv',
    '12602': 'dmax',
    '17501': 'pro7',
    '17502': 'kabel1',
    '17504': 'sat1gold',
    '17505': 'pro7maxx',
    '17509': 'kabel1_doku',
    '17772': 'tlc',
    '28006': 'zdf',
    '28007': '3sat',
    '28008': 'kika',
    '28011': 'zdf_info',
    '28014': 'zdf_neo'
};


rl.once('close', () => {

    let channels = Object.values(guide).map(channel => {
        let events = Object.values(channel.events).map(event => {
            let lang = event.lang ? `lang="${escapeXml(event.lang)}"`: "";

            return `<programme start="${format_time(event.unixTimeBegin)}" stop="${format_time(event.unixTimeEnd)}" channel="${channel.serviceId}">
    <title ${lang}>${escapeXml(event.name)}</title>
    ${event.desc ? `<desc ${lang}>${escapeXml(event.desc)}</desc>` : ''}
    <date>${format_date(event.unixTimeBegin)}</date>
    ${event.genre ? `<category ${lang}>${escapeXml(event.genre)}</category>` : ''}
</programme>`;
});

// <episode-num system="dd_progid">EP01006886.0028</episode-num>
// <episode-num system="onscreen">427</episode-num>


    let icon = icon_mapping[channel.serviceId] ? `<icon src="https://download.avm.de/tv/logos/${icon_mapping[channel.serviceId]}.png" />` : "";

    // if (!icon_mapping[channel.name]) process.stderr.write(`--- icon:${channel.serviceId}\t${channel.name}\n`);

        return `<channel id="${channel.serviceId}">
    <display-name>${escapeXml(channel.name)}</display-name>
    ${icon}
</channel>
${events.join('\n')}`;
    });
    console.log(`<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE tv SYSTEM "xmltv.dtd">
<tv source-info-name="DVB-C">
${channels.join('\n')}
</tv>`);
});