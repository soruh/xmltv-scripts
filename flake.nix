{
  description = "get epg data from rtsp";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-23.05;
  inputs.dvbtee.url = gitlab:soruh/nix-dvbtee/72a6b8ebd3777bacfa36b62aa09063fb32663a37;
  inputs.dvbtee.inputs.nixpkgs.follows = "nixpkgs";

  # https://github.com/mkrufky/libdvbtee/archive/refs/tags/v0.6.8.tar.gz

  outputs = { self, nixpkgs, dvbtee }: {
    defaultPackage.x86_64-linux = let pkgs = import nixpkgs { system = "x86_64-linux"; }; in
      pkgs.stdenv.mkDerivation {
        name = "get_epg";
        src = self;
        # buildPhase = '''';
        installPhase = ''
          mkdir -p $out/bin
          cat get_epg.sh | sed 's,openRTSP,${pkgs.live555}/bin/openRTSP,' | sed 's,dvbtee,${dvbtee.defaultPackage.x86_64-linux}/bin/dvbtee,' >$out/bin/get_epg.sh
          chmod +x $out/bin/get_epg.sh

          echo "#!${pkgs.nodejs_20}/bin/node" >$out/bin/convert_to_xml.js
          cat convert_to_xml.js >>$out/bin/convert_to_xml.js
          chmod +x $out/bin/convert_to_xml.js
        '';
        nativeBuildInputs = [ ];
        buildInputs = [ dvbtee.defaultPackage.x86_64-linux pkgs.live555 pkgs.nodejs_20 pkgs.curl ];
      };
  };
}